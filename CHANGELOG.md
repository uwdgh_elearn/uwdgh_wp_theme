# Changelog


## 3.3.1.14 / 2024‑12‑10

- Updated for parent theme version 3.3
- Removed shortcode features that have moved to the "UW WP Theme shortcode add-ons" plugin


## 3.0.1.13 / 2024‑02‑29

- Removed all non-required plugin recommendations from dependencies


## 3.0.1.12 / 2024‑02‑24

- Bug fix: dynamically resize height of sticky menu on small screens


## 3.0.1.11 / 2024‑02‑22

- CSS bug fixes


## 3.0.1.10 / 2024‑02‑21

- Added sticky header options


## 3.0.1.9 / 2024‑02‑12

- Cleanup dashboard for non-admins. Add WP resources widget


## 3.0.1.8 / 2024‑02‑08

- Banner text in Jumbotron template now uses the hashtag design similar to the Hero templates


## 3.0.1.7 / 2024‑02‑05

- Added [WP Dependency Installer](https://github.com/afragen/wp-dependency-installer)
- Customized Jumbotron Hero template
- Added custom shortcode udub_slant
- Code refactoring


## 3.0.1.6 / 2023‑12‑29

- Added option setting the title attribute on cards
- Removed option to disable Aplliction Passwords section from Profile page


## 3.0.0.5 / 2023‑11‑20

- Tested with uw_wp_theme version 3.0
- Text Domain update


## 3.0.0.4 / 2023‑11‑17

- ~~Tested with uw_wp_theme version 3.0~~
- Tested with WordPress 6.4
- Code refactoring using WordPress hooks


## 2.3.1.3 / 2023‑10‑31

- Tested with uw_wp_theme version 2.3
- Tested with PHP 8.2
- Style updates


## 2.1.1.2 / 2022‑12‑23

- Tested with uw_wp_theme version 2.1


## 1.0.1 / 2022‑07‑06

- Updated header.php with file from parent theme to fix conflicts with UW's structure updates


## 1.0 / 2022‑01‑28

- Initial release
- Added: Customized header and footer templates for the Department of Global Health


## 0.1 / 2021‑12‑21

- Initial commit
